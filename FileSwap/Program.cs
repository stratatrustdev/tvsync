﻿using System;
using System.IO;

namespace FileOverwrite
{
    class Program
    {
        static void Main(string[] args)
        {
            var argsException = "Please supply two fully-qualified file names:  first the file to copy from, and second the file to overwrite.";

            if (args is null)
            {
                throw new ArgumentNullException("args", "Argument list is null. " + argsException);
            }
            else if (args.Length != 2)
            {
                throw new ArgumentOutOfRangeException("args", "Incorrect number of arguments supplied.  " + argsException);
            }
            else
            {
                for (var i = 0; i <= 1; i++)
                {
                    if (args[i] is null)
                    {
                        throw new ArgumentNullException("Argument " + i, "An argument to this procedure is null. " + argsException);
                    }
                    else if (!File.Exists(args[i]))
                    {
                        throw new FileNotFoundException("A file was declared that does not exist.", args[i]);
                    }
                }

                File.Copy(args[0], args[1], true);
            }
        }
    }
}
